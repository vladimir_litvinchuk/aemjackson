$(function() {
      $("#subutton").click( function()
           {
                var user = collectToJson($("#jsonForm").find('.myclass'));
                var adress = collectToJson($("#jsonForm").find('.myadress'));
                $.ajax({
                    type:'get',
                    url:'/bin/jackson/example/servlet',
                    dataType: 'text',
                    data: {'user':user, 'adress':adress},
                    success: function(data) {
                        $(".result").html("And response is" + data);
                    },
                    error: function() {
                        alert('Not OKay');
                    }
                });

           }
      );
});

function collectToJson(elements) {
    var obj = {};
    for( var i = 0; i < elements.length; ++i ) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;
        if(name) {
            obj[name] = value;
        }
    }
    return JSON.stringify(obj);
}

