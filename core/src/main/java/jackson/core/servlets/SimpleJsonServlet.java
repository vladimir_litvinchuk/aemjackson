package jackson.core.servlets;

import jackson.core.logic.JsonProcessor;
import jackson.core.logic.RecomendationProcessor;
import jackson.core.logic.beans.User;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.text.MessageFormat;


@Component(service=Servlet.class,
           property={
                   Constants.SERVICE_DESCRIPTION + "=SimpleJsonServlet",
                   "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                   "sling.servlet.paths="+ "/bin/jackson/example/servlet",
           })
public class SimpleJsonServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUid = 1L;

    private static Logger logger = LoggerFactory.getLogger(SimpleJsonServlet.class);

    private static String PARM_USER = "user";
    private static String PARM_ADRESS = "adress";
    private static String CONTENT_TYPE = "text/plain";
    private static String CHAR_ECODING = "UTF-8";

    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
        logger.info("Start request processing");

        String jsonUser = req.getRequestParameter(PARM_USER).getString();
        String jsonUserAdress = req.getRequestParameter(PARM_ADRESS).getString();
        User newUser = JsonProcessor.jsonToUserClass(jsonUser, jsonUserAdress);
        String targetJson = JsonProcessor.classToJson(new RecomendationProcessor().getRecomendations(newUser));

        logger.info(MessageFormat.format("Response string - {0}", targetJson));

        resp.setContentType(CONTENT_TYPE);
        resp.setCharacterEncoding(CHAR_ECODING);
        resp.getWriter().write(targetJson);

        logger.info("Request processing was finished");
    }

}
