package jackson.core.logic.beans;

import java.util.ArrayList;
import java.util.List;

public class Recomendations {

    private List<Product> products;

    public Recomendations(List<Product> products) {
        this.products = products;
    }

    public Recomendations() {
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Recomendations{" +
                "products=" + products +
                '}';
    }
}
