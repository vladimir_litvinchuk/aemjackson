package jackson.core.logic.beans;

public class User {
    private String name;
    private int age;
    private String profession;
    private Adress adress;
    private Recomendations recomendations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public Recomendations getRecomendations() {
        return recomendations;
    }

    public void setRecomendations(Recomendations recomendations) {
        this.recomendations = recomendations;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", profession='" + profession + '\'' +
                ", adress=" + adress +
                ", recomendations=" + recomendations +
                '}';
    }
}
