package jackson.core.logic.beans;

import java.util.HashMap;
import java.util.Map;

public class Discount {
    private Map<String,String> subsDiscount;

    {
        subsDiscount = new HashMap<String,String>();
        subsDiscount.put("Month","2%");
        subsDiscount.put("Year","5%");
        subsDiscount.put("more","10%");
        subsDiscount.put("buy","7%");
    }

    public Map<String, String> getSubsDiscount() {
        return subsDiscount;
    }

    public void setSubsDiscount(Map<String, String> subsDiscount) {
        this.subsDiscount = subsDiscount;
    }
}
