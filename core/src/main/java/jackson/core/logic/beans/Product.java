package jackson.core.logic.beans;

public class Product {
    private String productName;
    private int price;
    private Discount discount;

    public Product(String productName, int price, Discount discount) {
        this.productName = productName;
        this.price = price;
        this.discount = discount;
    }

    public Product(String productName, int price) {
        this.productName = productName;
        this.price = price;
    }

    public Product() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }
}
