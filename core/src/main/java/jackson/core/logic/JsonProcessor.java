package jackson.core.logic;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jackson.core.logic.beans.Adress;
import jackson.core.logic.beans.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;

public class JsonProcessor {
    private static Logger logger = LoggerFactory.getLogger(JsonProcessor.class);
    private static ObjectMapper objectMapper = new ObjectMapper(); // only one place where Jackson was used

    public static User jsonToUserClass (final String userjson, final String adressjson){
        logger.info(MessageFormat.format(
                "Json to user process started: userjson - {0}, adressjson - {1}",userjson, adressjson));
        User newUser = null;
        Adress newAdress = null;
        try {
            newUser = objectMapper.readValue(userjson, User.class);
            newAdress = objectMapper.readValue(adressjson, Adress.class);
            newUser.setAdress(newAdress);
        } catch (IOException e) {
            logger.error(e.getStackTrace().toString());
        }
        logger.info(MessageFormat.format("Json to user process finished: user - {0}", newUser));
        return newUser;
    }

    public static String classToJson (final User user){
        String jsonUser = null;
        try {
            jsonUser = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            logger.error(e.getStackTrace().toString());
        }
        return jsonUser;
    }
}
