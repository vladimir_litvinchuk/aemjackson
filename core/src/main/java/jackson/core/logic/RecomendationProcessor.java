package jackson.core.logic;

import jackson.core.logic.beans.Discount;
import jackson.core.logic.beans.Product;
import jackson.core.logic.beans.Recomendations;
import jackson.core.logic.beans.User;
import jackson.core.servlets.SimpleJsonServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RecomendationProcessor {

    private static Logger logger = LoggerFactory.getLogger(RecomendationProcessor.class);

    public User getRecomendations(User user){
        List<Product> products = new ArrayList<>();
        switch (user.getProfession()){
            case "Constructor":
                products.add(getAutocad());
              break;
            case "Architector":
                products.add(getRevit());
                break;
            case "3dModeling":
                products.add(getMaya());
                break;
            default:
                products.add(getAutocad());
                products.add(getRevit());
                products.add(getMaya());
                break;
        }
            user.setRecomendations(new Recomendations(products));
        return user;
    }
    private Product getAutocad(){
        return new Product("Autocad", 120, new Discount());
    }
    private Product getRevit(){
        return new Product("Revit", 220, new Discount());
    }
    private Product getMaya(){
        return new Product("Maya", 250);
    }
}
