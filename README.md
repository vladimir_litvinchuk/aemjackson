# Project
Example for Jackson lib

## Modules

* core: Java bundle containing all main functionality
* exporter: Java bundle which contains Jackson libs, and export them
* ui.apps: contains page template, one component with form and clientlibs
* ui.content: already created example page

## How to build
    mvn clean install
## Short behavior description
Form's data is sent to the server via the AJAX request, where it is transferred to the 
Java object, based on the profession, recommended products are added with the terms of purchase,
the ready object is transferred to the valid json string and displayed under the form on the same page.